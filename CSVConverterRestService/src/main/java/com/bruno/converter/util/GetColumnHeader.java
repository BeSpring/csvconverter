package com.bruno.converter.util;

import java.util.ArrayList;
import java.util.List;


public class GetColumnHeader {
	public List<String> getHeader(Object[] headers) {
		List<String> intSheets = new ArrayList<>();
		for (int i = 0; i < headers.length; i++) {
			intSheets.add(headers[i].toString());
		}

		return intSheets;

	}

}
