package com.bruno.converter.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonParse {
	public JSONObject parseJson(String fileJson) throws IOException {
		Reader fileReader = null;
		JSONObject parse = null;
		JSONParser parser = new JSONParser();
		try {
			fileReader = new FileReader(new File(fileJson));
			parse = (JSONObject) parser.parse(fileReader);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (ParseException e) {
			System.out.println(e.toString());
		}

		return parse;
	}

}
