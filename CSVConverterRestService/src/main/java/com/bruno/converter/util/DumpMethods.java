//package com.bruno.converter.util;
//
//import java.lang.reflect.Method;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.List;
//
///**
// * Compile with this: C:\Documents and Settings\glow\My Documents\j>javac
// * DumpMethods.java
// * 
// * Run like this, and results follow C:\Documents and Settings\glow\My
// * Documents\j>java DumpMethods public void DumpMethods.foo() public int
// * DumpMethods.bar() public java.lang.String DumpMethods.baz() public static
// * void DumpMethods.main(java.lang.String[])
// */
//
//public class DumpMethods {
//
//	public void foo() {
//	}
//
//	public int bar() {
//		return 12;
//	}
//
//	public String baz() {
//		return "";
//	}
//
//	public static void main(String args[]) {
//		try {
//			Object obj = new Object();
//			Class<?> c = obj.getClass();
//			Method[] m = c.getDeclaredMethods();
//			System.out.println(m[5].invoke(obj));
//			List<String> methods = new ArrayList<>();
//			for (int i = 0; i < m.length; i++)
////            	if(m[i].getName().contains("to"))
//				methods.add(m[i].getName());
////			System.out.println(m[i].getName());
//			Collections.sort(methods);
//			System.out.println(methods.toString());
//
//		} catch (Throwable e) {
//			System.err.println(e);
//		}
//	}
//}
