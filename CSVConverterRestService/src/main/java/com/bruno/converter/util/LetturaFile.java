package com.bruno.converter.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class LetturaFile {
	public static void main(String[] args) throws IOException {
		
		
		
		byte[] buffer = new byte[4096]; // 4KB di buffer...
		int byteLetti = 0;
		// Apro il file di origine
		FileInputStream fis = new FileInputStream("C:/Users/Bruno Baldassare/MavenPOI/GenerateCSVExcel/tmp/MyFirstExcel.xlsx");
		// Apro il file di destinazione
//		FileOutputStream fos = new FileOutputStream( "//HOST_DST/destinazione/file di destinazione" );
		FileOutputStream fos = new FileOutputStream( "C:/Users/Bruno Baldassare/Desktop/sample.xlsx" );
		
		
		// Leggo e scrivo
		while((byteLetti = fis.read(buffer)) >= 0) {
		fos.write(buffer, 0, byteLetti);
		}

		// Chiudo il file di destinazione
		fos.close();

		// Chiudo il file di origine
		fis.close();
//		try {
//			FileOutputStream file = new FileOutputStream("/opt/TEST/file.txt");
//
//			byte[] buffer = new byte[4096];
//			int byteLetti = 0;
//
//			FileInputStream fis = new FileInputStream("C:/prova_trasferimento.txt");
//
//			while ((byteLetti = fis.read(buffer)) >= 0) {
//				file.write(buffer, 0, byteLetti);
//			}
//
//// Chiudo il file di destinazione
//			file.close();
//
//// Chiudo il file di origine
//			fis.close();
//
//		} catch (IOException e) {
//			System.out.println("Errore: " + e);
//			System.exit(1);
//		}

	}
}
