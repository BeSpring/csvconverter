package com.bruno.converter.services.physicaladdress.beans;

import java.util.List;

import com.bruno.converter.services.beans.PhysicalAddress;

public class PhysicalAddressOutputBean {
	private List<PhysicalAddress> physicalAddresses;

	public List<PhysicalAddress> getPhysicalAddresses() {
		return physicalAddresses;
	}

	public void setPhysicalAddresses(List<PhysicalAddress> physicalAddresses) {
		this.physicalAddresses = physicalAddresses;
	}

}
