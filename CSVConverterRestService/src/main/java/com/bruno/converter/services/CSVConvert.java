package com.bruno.converter.services;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.bruno.createfile.util.CSVUtils;

public class CSVConvert {
	public String createCSV(JSONObject jsonObject, String destFolder) throws IOException {
		List<List<String>> rows = new ArrayList<>();

		if (jsonObject != null) {
			Long studentId = (Long) jsonObject.get("ndg");
			System.out.println(studentId);

			JSONArray array = (JSONArray) jsonObject.get("sheetList");
			List<String> sheets = new ArrayList<>();
			for (int i = 0; i < array.size(); i++) {
				sheets.add((String) array.get(i));
			}

			JSONArray array22 = (JSONArray) jsonObject.get("Anagrafica di base");
			for (int i = 0; i < array22.size(); i++) {
				List<String> li = new ArrayList<String>();
				JSONObject obj1 = (JSONObject) array22.get(i);
				System.out.println(array22.get(i).toString());
				StringBuilder stringBuilder = new StringBuilder();
				for (int j = 1; j < obj1.size(); j++) {

					String s = (String) obj1.get("col" + j);
					stringBuilder.append(s + ";");

				}
				li.add(String.valueOf(stringBuilder));
				rows.add(li);

			}
		}

		FileWriter writer = new FileWriter(destFolder);
		for (List<String> s : rows) {
			for (String str : s) {
				List<String> list = new ArrayList<>();
				list.add(str);
				CSVUtils.writeLine(writer, list);
				list.clear();
			}

		}

		writer.flush();
		writer.close();

		return "File created in " + destFolder;

	}
}
