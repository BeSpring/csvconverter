package com.bruno.converter.services.protestsprejudices.beans;

public class ProtestsPrejudices {
	private String uniqueId;
	private String protest;
	private String defaults;
	private String sufferings;
	private String prejudices;
	private String complaints;
	private String lawsuitsInProgress;

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getProtest() {
		return protest;
	}

	public void setProtest(String protest) {
		this.protest = protest;
	}

	public String getDefaults() {
		return defaults;
	}

	public void setDefaults(String defaults) {
		this.defaults = defaults;
	}

	public String getSufferings() {
		return sufferings;
	}

	public void setSufferings(String sufferings) {
		this.sufferings = sufferings;
	}

	public String getPrejudices() {
		return prejudices;
	}

	public void setPrejudices(String prejudices) {
		this.prejudices = prejudices;
	}

	public String getComplaints() {
		return complaints;
	}

	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}

	public String getLawsuitsInProgress() {
		return lawsuitsInProgress;
	}

	public void setLawsuitsInProgress(String lawsuitsInProgress) {
		this.lawsuitsInProgress = lawsuitsInProgress;
	}

}
