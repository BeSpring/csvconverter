package com.bruno.converter.services.family.beans;

public class Family {

	private String familyMembers;
	private String familyMembersIncome;
	private String dependentFamilyMembers;
	private String otherDependents;
	private String family;
	private String dependentChildren;
	private String sons;
	
	public Family(String familyMembers, String familyMembersIncome, String dependentFamilyMembers,
			String otherDependents, String family, String dependentChildren, String sons) {
		super();
		this.familyMembers = familyMembers;
		this.familyMembersIncome = familyMembersIncome;
		this.dependentFamilyMembers = dependentFamilyMembers;
		this.otherDependents = otherDependents;
		this.family = family;
		this.dependentChildren = dependentChildren;
		this.sons = sons;
	}
	
	public String getFamilyMembers() {
		return familyMembers;
	}
	public void setFamilyMembers(String familyMembers) {
		this.familyMembers = familyMembers;
	}
	public String getFamilyMembersIncome() {
		return familyMembersIncome;
	}
	public void setFamilyMembersIncome(String familyMembersIncome) {
		this.familyMembersIncome = familyMembersIncome;
	}
	public String getDependentFamilyMembers() {
		return dependentFamilyMembers;
	}
	public void setDependentFamilyMembers(String dependentFamilyMembers) {
		this.dependentFamilyMembers = dependentFamilyMembers;
	}
	public String getOtherDependents() {
		return otherDependents;
	}
	public void setOtherDependents(String otherDependents) {
		this.otherDependents = otherDependents;
	}
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public String getDependentChildren() {
		return dependentChildren;
	}
	public void setDependentChildren(String dependentChildren) {
		this.dependentChildren = dependentChildren;
	}
	public String getSons() {
		return sons;
	}
	public void setSons(String sons) {
		this.sons = sons;
	}
	
	
}
