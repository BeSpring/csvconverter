package com.bruno.converter.services.historicalcontact.beans;

import java.util.List;

import com.bruno.converter.services.beans.HistoricalContact;

public class HistoricalContactOutputBean {
	private List<HistoricalContact> historicalContacts;

	public List<HistoricalContact> getHistoricalContacts() {
		return historicalContacts;
	}

	public void setHistoricalContacts(List<HistoricalContact> historicalContacts) {
		this.historicalContacts = historicalContacts;
	}

}
