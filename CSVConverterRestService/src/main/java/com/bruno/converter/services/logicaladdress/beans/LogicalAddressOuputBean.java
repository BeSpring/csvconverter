package com.bruno.converter.services.logicaladdress.beans;

import java.util.List;

import com.bruno.converter.services.beans.LogicalAddress;

public class LogicalAddressOuputBean {
	private List<LogicalAddress> logicalAddresses;

	public List<LogicalAddress> getLogicalAddresses() {
		return logicalAddresses;
	}

	public void setLogicalAddresses(List<LogicalAddress> logicalAddresses) {
		this.logicalAddresses = logicalAddresses;
	}

}
