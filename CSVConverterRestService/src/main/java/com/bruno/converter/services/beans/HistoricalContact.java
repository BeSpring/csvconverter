package com.bruno.converter.services.beans;

public class HistoricalContact {

	private String date;
	private String type;
	private String event;
	private String object;
	private String result;
	
	
	public HistoricalContact(String date, String type, String event, String object, String result) {
		super();
		this.date = date;
		this.type = type;
		this.event = event;
		this.object = object;
		this.result = result;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	
}
