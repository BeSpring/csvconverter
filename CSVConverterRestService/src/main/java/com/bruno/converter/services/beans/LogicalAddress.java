package com.bruno.converter.services.beans;

public class LogicalAddress {
	private String type;
	private String address;

	public LogicalAddress(String type, String address) {
		super();
		this.type = type;
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
