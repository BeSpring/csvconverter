package com.bruno.converter.services.headers.ana;

public enum DocumentHeaders {

	TIPO_DI_DOCUMENTO, N_DOCUMENTO, NAZIONALITÀ, LUOGO_DI_EMISSIONE, DATA_EMISSIONE, DATA_SCADENZA, ENTE_DI_EMISSIONE;

}
