package com.bruno.converter.services.document.beans;

import java.util.List;

import com.bruno.converter.services.beans.Document;

public class DocumentOutputBean {
	private List<Document> documents;

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

}
