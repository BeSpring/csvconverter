package com.bruno.converter.services.privacy.beans;

public class Privacy {
	private String privacyPolicyDelivered;
	private String privacyPolicyCode;
	private String privacyPolicyVersion;
	private String profilingTest;
	private String internalMarketingTest;
	private String internalMarketingExternal;
	private String profilingTestBis;
	private String internalMarketingTestBis;
	private String internalMarketingExternalBis;

	public String getPrivacyPolicyDelivered() {
		return privacyPolicyDelivered;
	}

	public void setPrivacyPolicyDelivered(String privacyPolicyDelivered) {
		this.privacyPolicyDelivered = privacyPolicyDelivered;
	}

	public String getPrivacyPolicyCode() {
		return privacyPolicyCode;
	}

	public void setPrivacyPolicyCode(String privacyPolicyCode) {
		this.privacyPolicyCode = privacyPolicyCode;
	}

	public String getPrivacyPolicyVersion() {
		return privacyPolicyVersion;
	}

	public void setPrivacyPolicyVersion(String privacyPolicyVersion) {
		this.privacyPolicyVersion = privacyPolicyVersion;
	}

	public String getProfilingTest() {
		return profilingTest;
	}

	public void setProfilingTest(String profilingTest) {
		this.profilingTest = profilingTest;
	}

	public String getInternalMarketingTest() {
		return internalMarketingTest;
	}

	public void setInternalMarketingTest(String internalMarketingTest) {
		this.internalMarketingTest = internalMarketingTest;
	}

	public String getInternalMarketingExternal() {
		return internalMarketingExternal;
	}

	public void setInternalMarketingExternal(String internalMarketingExternal) {
		this.internalMarketingExternal = internalMarketingExternal;
	}

	public String getProfilingTestBis() {
		return profilingTestBis;
	}

	public void setProfilingTestBis(String profilingTestBis) {
		this.profilingTestBis = profilingTestBis;
	}

	public String getInternalMarketingTestBis() {
		return internalMarketingTestBis;
	}

	public void setInternalMarketingTestBis(String internalMarketingTestBis) {
		this.internalMarketingTestBis = internalMarketingTestBis;
	}

	public String getInternalMarketingExternalBis() {
		return internalMarketingExternalBis;
	}

	public void setInternalMarketingExternalBis(String internalMarketingExternalBis) {
		this.internalMarketingExternalBis = internalMarketingExternalBis;
	}

}
