package com.bruno.converter.services.registry;

import org.json.simple.JSONObject;

import com.bruno.converter.services.registry.beans.Registry;

public class RegistryService {
	public Registry callRegistryService(JSONObject jsonObject) {

		Registry registry = new Registry();
		registry.setExtendedName((String) jsonObject.get("extendedName"));
		registry.setUniqueId((String) jsonObject.get("uniqueId"));
		registry.setTaxCode((String) jsonObject.get("taxCode"));
		registry.setGender((String) jsonObject.get("gender"));
		registry.setType((String) jsonObject.get("type"));
		registry.setMatchTypes((String) jsonObject.get("matchTypes"));
		registry.setDateBirth((String) jsonObject.get("dateBirth"));
		registry.setBirthplace((String) jsonObject.get("birthplace"));
		registry.setCitizenship((String) jsonObject.get("citizenship"));
		registry.setCompanyName((String) jsonObject.get("companyName"));
		registry.setVatNumber((String) jsonObject.get("vatNumber"));
		registry.setNoResidentCode((String) jsonObject.get("noResidentCode"));
		registry.setMaritalStatus((String) jsonObject.get("maritalStatus"));
		registry.setSurnameAcquired((String) jsonObject.get("surnameAcquired"));
		registry.setMatrimonialRegime((String) jsonObject.get("matrimonialRegime"));
		registry.setNickname((String) jsonObject.get("nickname"));
		registry.setStateOfLife((String) jsonObject.get("stateOfLife"));
		registry.setDateDeath((String) jsonObject.get("dateDeath"));
		return registry;
	}

}
