package com.bruno.converter.services.beans;

public class RelatedPerson {
	private String relatedPerson;
	private String type;
	private String uniqueId;
	private String rapportId;
	private String validityStartDate;
	private String endDateValidity;
	
	public RelatedPerson(String relatedPerson, String type, String uniqueId, String rapportId, String validityStartDate,
			String endDateValidity) {
		super();
		this.relatedPerson = relatedPerson;
		this.type = type;
		this.uniqueId = uniqueId;
		this.rapportId = rapportId;
		this.validityStartDate = validityStartDate;
		this.endDateValidity = endDateValidity;
	}

	public String getRelatedPerson() {
		return relatedPerson;
	}

	public void setRelatedPerson(String relatedPerson) {
		this.relatedPerson = relatedPerson;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getRapportId() {
		return rapportId;
	}

	public void setRapportId(String rapportId) {
		this.rapportId = rapportId;
	}

	public String getValidityStartDate() {
		return validityStartDate;
	}

	public void setValidityStartDate(String validityStartDate) {
		this.validityStartDate = validityStartDate;
	}

	public String getEndDateValidity() {
		return endDateValidity;
	}

	public void setEndDateValidity(String endDateValidity) {
		this.endDateValidity = endDateValidity;
	}

}
