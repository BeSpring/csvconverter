package com.bruno.converter.services.beans;

public class LicensePlate {

	private String licensePlate;
	private String idRelated;
	private String validityStartDate;
	private String endDateValidity;
	
	public LicensePlate(String licensePlate, String idRelated, String validityStartDate, String endDateValidity) {
		super();
		this.licensePlate = licensePlate;
		this.idRelated = idRelated;
		this.validityStartDate = validityStartDate;
		this.endDateValidity = endDateValidity;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getIdRelated() {
		return idRelated;
	}

	public void setIdRelated(String idRelated) {
		this.idRelated = idRelated;
	}

	public String getValidityStartDate() {
		return validityStartDate;
	}

	public void setValidityStartDate(String validityStartDate) {
		this.validityStartDate = validityStartDate;
	}

	public String getEndDateValidity() {
		return endDateValidity;
	}

	public void setEndDateValidity(String endDateValidity) {
		this.endDateValidity = endDateValidity;
	}

}
