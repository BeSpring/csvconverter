package com.bruno.converter.services.relatedperson.beans;

import java.util.List;

import com.bruno.converter.services.beans.RelatedPerson;

public class RelatedPersonOutputBean {
	private List<RelatedPerson> relatedPersons;

	public List<RelatedPerson> getRelatedPersons() {
		return relatedPersons;
	}

	public void setRelatedPersons(List<RelatedPerson> relatedPersons) {
		this.relatedPersons = relatedPersons;
	}

}
