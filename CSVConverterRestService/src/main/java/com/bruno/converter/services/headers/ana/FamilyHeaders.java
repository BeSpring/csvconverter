package com.bruno.converter.services.headers.ana;

public enum FamilyHeaders {
	FAMILIARI_TOTALI, FAMILIARI_CON_REDDITO, FAMILIARI_A_CARICO, ALTRI_FAMILIARI_A_CARICO, NUCLEO_FAMILIARE,
	FIGLI_A_CARICO, FIGLI;

}
