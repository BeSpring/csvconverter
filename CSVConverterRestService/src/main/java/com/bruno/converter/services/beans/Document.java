package com.bruno.converter.services.beans;

public class Document {

	private String type;
	private String number;
	private String nationality;
	private String placeOfIssue;
	private String dateOfIssue;
	private String dateExpiration;
	private String issuingBody;
	
	public Document(String type, String number, String nationality, String placeOfIssue, String dateOfIssue,
			String dateExpiration, String issuingBody) {
		super();
		this.type = type;
		this.number = number;
		this.nationality = nationality;
		this.placeOfIssue = placeOfIssue;
		this.dateOfIssue = dateOfIssue;
		this.dateExpiration = dateExpiration;
		this.issuingBody = issuingBody;
	}
	
	public Document() {
		// TODO Auto-generated constructor stub
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPlaceOfIssue() {
		return placeOfIssue;
	}
	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}
	public String getDateOfIssue() {
		return dateOfIssue;
	}
	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}
	public String getDateExpiration() {
		return dateExpiration;
	}
	public void setDateExpiration(String dateExpiration) {
		this.dateExpiration = dateExpiration;
	}
	public String getIssuingBody() {
		return issuingBody;
	}
	public void setIssuingBody(String issuingBody) {
		this.issuingBody = issuingBody;
	}
	
	
}
