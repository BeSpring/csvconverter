package com.bruno.converter.services.registry.beans;

public class Registry {

	private String type ;
	private String extendedName ;
	private String uniqueId ;   //ndg o id persona
	private String taxCode ;
	private String vatNumber ;
	private String gender ;
	private String matchTypes ;
	private String noResidentCode ;
	private String dateBirth ;
	private String citizenship ;
	private String companyName ;
	private String maritalStatus ;
	private String surnameAcquired ;
	private String matrimonialRegime ;
	private String nickname ;
	private String stateOfLife ;
	private String dateDeath ;
	private String birthplace ;
	
	
	
	public Registry(String type, String extendedName, String uniqueId, String taxCode, String vatNumber, String gender,
			String matchTypes, String noResidentCode, String dateBirth, String citizenship, String companyName,
			String maritalStatus, String surnameAcquired, String matrimonialRegime, String nickname, String stateOfLife,
			String dateDeath, String birthplace) {
		super();
		this.type = type;
		this.extendedName = extendedName;
		this.uniqueId = uniqueId;
		this.taxCode = taxCode;
		this.vatNumber = vatNumber;
		this.gender = gender;
		this.matchTypes = matchTypes;
		this.noResidentCode = noResidentCode;
		this.dateBirth = dateBirth;
		this.citizenship = citizenship;
		this.companyName = companyName;
		this.maritalStatus = maritalStatus;
		this.surnameAcquired = surnameAcquired;
		this.matrimonialRegime = matrimonialRegime;
		this.nickname = nickname;
		this.stateOfLife = stateOfLife;
		this.dateDeath = dateDeath;
		this.birthplace = birthplace;
	}
	public Registry() {

	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getExtendedName() {
		return extendedName;
	}
	public void setExtendedName(String extendedName) {
		this.extendedName = extendedName;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getTaxCode() {
		return taxCode;
	}
	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMatchTypes() {
		return matchTypes;
	}
	public void setMatchTypes(String matchTypes) {
		this.matchTypes = matchTypes;
	}
	public String getNoResidentCode() {
		return noResidentCode;
	}
	public void setNoResidentCode(String noResidentCode) {
		this.noResidentCode = noResidentCode;
	}
	public String getDateBirth() {
		return dateBirth;
	}
	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}
	public String getCitizenship() {
		return citizenship;
	}
	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getSurnameAcquired() {
		return surnameAcquired;
	}
	public void setSurnameAcquired(String surnameAcquired) {
		this.surnameAcquired = surnameAcquired;
	}
	public String getMatrimonialRegime() {
		return matrimonialRegime;
	}
	public void setMatrimonialRegime(String matrimonialRegime) {
		this.matrimonialRegime = matrimonialRegime;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getStateOfLife() {
		return stateOfLife;
	}
	public void setStateOfLife(String stateOfLife) {
		this.stateOfLife = stateOfLife;
	}
	public String getDateDeath() {
		return dateDeath;
	}
	public void setDateDeath(String dateDeath) {
		this.dateDeath = dateDeath;
	}
	public String getBirthplace() {
		return birthplace;
	}
	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}
	@Override
	public String toString() {
		return "Registry [type=" + type + ", extendedName=" + extendedName + ", uniqueId=" + uniqueId + ", taxCode="
				+ taxCode + ", vatNumber=" + vatNumber + ", gender=" + gender + ", matchTypes=" + matchTypes
				+ ", noResidentCode=" + noResidentCode + ", dateBirth=" + dateBirth + ", citizenship=" + citizenship
				+ ", companyName=" + companyName + ", maritalStatus=" + maritalStatus + ", surnameAcquired="
				+ surnameAcquired + ", matrimonialRegime=" + matrimonialRegime + ", nickname=" + nickname
				+ ", stateOfLife=" + stateOfLife + ", dateDeath=" + dateDeath + ", birthplace=" + birthplace + "]";
	}
	

	
}
