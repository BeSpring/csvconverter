package com.bruno.converter.services.beans;

public class PhysicalAddress {

	private String type;
	private String address1;
	private String address2;
	private String country;
	private String rapportId;
	private String validityStartDate;
	private String endDateValidity;

	public PhysicalAddress(String type, String address1, String address2, String country, String rapportId,
			String validityStartDate, String endDateValidity) {
		super();
		this.type = type;
		this.address1 = address1;
		this.address2 = address2;
		this.country = country;
		this.rapportId = rapportId;
		this.validityStartDate = validityStartDate;
		this.endDateValidity = endDateValidity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRapportId() {
		return rapportId;
	}

	public void setRapportId(String rapportId) {
		this.rapportId = rapportId;
	}

	public String getValidityStartDate() {
		return validityStartDate;
	}

	public void setValidityStartDate(String validityStartDate) {
		this.validityStartDate = validityStartDate;
	}

	public String getEndDateValidity() {
		return endDateValidity;
	}

	public void setEndDateValidity(String endDateValidity) {
		this.endDateValidity = endDateValidity;
	}

}
