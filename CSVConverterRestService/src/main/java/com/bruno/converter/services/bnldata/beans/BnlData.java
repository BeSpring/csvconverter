package com.bruno.converter.services.bnldata.beans;

public class BnlData {

	private String signature;
	private String sourceCodeCensus;
	private String descSourceCensus;
	private String censusSource;
	private String registryCluster;
	private String reportStatus;
	private String customerStatus;
	private String type;
	private String counterOfficeCode;
	private String mainDoorCode;
	private String dateAcquisition;
	private String bnlSector;
	private String problems;
	private String managementProblem;
	private String market;
	private String macroSegment;
	private String segment;
	private String uo;
	private String descUo;
	private String manager;
	
	public BnlData(String signature, String sourceCodeCensus, String descSourceCensus, String censusSource,
			String registryCluster, String reportStatus, String customerStatus, String type, String counterOfficeCode,
			String mainDoorCode, String dateAcquisition, String bnlSector, String problems, String managementProblem,
			String market, String macroSegment, String segment, String uo, String descUo, String manager) {
		super();
		this.signature = signature;
		this.sourceCodeCensus = sourceCodeCensus;
		this.descSourceCensus = descSourceCensus;
		this.censusSource = censusSource;
		this.registryCluster = registryCluster;
		this.reportStatus = reportStatus;
		this.customerStatus = customerStatus;
		this.type = type;
		this.counterOfficeCode = counterOfficeCode;
		this.mainDoorCode = mainDoorCode;
		this.dateAcquisition = dateAcquisition;
		this.bnlSector = bnlSector;
		this.problems = problems;
		this.managementProblem = managementProblem;
		this.market = market;
		this.macroSegment = macroSegment;
		this.segment = segment;
		this.uo = uo;
		this.descUo = descUo;
		this.manager = manager;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getSourceCodeCensus() {
		return sourceCodeCensus;
	}
	public void setSourceCodeCensus(String sourceCodeCensus) {
		this.sourceCodeCensus = sourceCodeCensus;
	}
	public String getDescSourceCensus() {
		return descSourceCensus;
	}
	public void setDescSourceCensus(String descSourceCensus) {
		this.descSourceCensus = descSourceCensus;
	}
	public String getCensusSource() {
		return censusSource;
	}
	public void setCensusSource(String censusSource) {
		this.censusSource = censusSource;
	}
	public String getRegistryCluster() {
		return registryCluster;
	}
	public void setRegistryCluster(String registryCluster) {
		this.registryCluster = registryCluster;
	}
	public String getReportStatus() {
		return reportStatus;
	}
	public void setReportStatus(String reportStatus) {
		this.reportStatus = reportStatus;
	}
	public String getCustomerStatus() {
		return customerStatus;
	}
	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCounterOfficeCode() {
		return counterOfficeCode;
	}
	public void setCounterOfficeCode(String counterOfficeCode) {
		this.counterOfficeCode = counterOfficeCode;
	}
	public String getMainDoorCode() {
		return mainDoorCode;
	}
	public void setMainDoorCode(String mainDoorCode) {
		this.mainDoorCode = mainDoorCode;
	}
	public String getDateAcquisition() {
		return dateAcquisition;
	}
	public void setDateAcquisition(String dateAcquisition) {
		this.dateAcquisition = dateAcquisition;
	}
	public String getBnlSector() {
		return bnlSector;
	}
	public void setBnlSector(String bnlSector) {
		this.bnlSector = bnlSector;
	}
	public String getProblems() {
		return problems;
	}
	public void setProblems(String problems) {
		this.problems = problems;
	}
	public String getManagementProblem() {
		return managementProblem;
	}
	public void setManagementProblem(String managementProblem) {
		this.managementProblem = managementProblem;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getMacroSegment() {
		return macroSegment;
	}
	public void setMacroSegment(String macroSegment) {
		this.macroSegment = macroSegment;
	}
	public String getSegment() {
		return segment;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public String getUo() {
		return uo;
	}
	public void setUo(String uo) {
		this.uo = uo;
	}
	public String getDescUo() {
		return descUo;
	}
	public void setDescUo(String descUo) {
		this.descUo = descUo;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	
	
}
