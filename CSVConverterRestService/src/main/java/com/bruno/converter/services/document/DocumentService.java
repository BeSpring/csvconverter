package com.bruno.converter.services.document;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

import com.bruno.converter.services.beans.Document;
import com.bruno.converter.services.document.beans.DocumentOutputBean;

public class DocumentService {
	public DocumentOutputBean callDocumentService(JSONObject jsonObject) {
		Document document = new Document();
		List<Document> documents = new ArrayList<>();

		DocumentOutputBean documentOutputBean = new DocumentOutputBean();
		document.setDateExpiration((String) jsonObject.get("dateExpiration"));
		document.setDateOfIssue((String) jsonObject.get("dateOfIssue"));
		document.setIssuingBody((String) jsonObject.get("issuingBody"));
		document.setNationality((String) jsonObject.get("nationality"));
		document.setNumber((String) jsonObject.get("number"));
		document.setPlaceOfIssue((String) jsonObject.get("placeOfIssue"));
		document.setType((String) jsonObject.get("type"));

		documents.add(document);
		documentOutputBean.setDocuments(documents);
		return documentOutputBean;
	}

}
