package com.bruno.converter.services.headers.ana;

public enum BnlDataHeaders {
	FIRMA_SPECIMEN, COD_FONTE_CENSIMENTO, DESCR_FONTE_CENSIMENTO, PROVENIENZA_CENSIMENTO, CLUSTER_ANAGRAFICO,
	STATO_RAPPORTO_BNL, STATO_CLIENTE, TIPO_ORGANIZZAZIONE, COD_SPORTELLO_CENSIMENTO, COD_SPORTELLO_PRINCIPALE,
	DATA_ACQUISIZIONE, SETTORE_BNL, POS_PROBLEMATICA, POS_PROBLEMATICA_GESTIONALE, MERCATO, MACROSEGMENTO, SEGMENTO, UO,
	DESCRIZIONE_UO, GESTORE;

}
