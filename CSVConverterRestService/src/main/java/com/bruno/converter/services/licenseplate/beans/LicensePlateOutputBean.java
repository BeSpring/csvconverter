package com.bruno.converter.services.licenseplate.beans;

import java.util.List;

import com.bruno.converter.services.beans.LicensePlate;

public class LicensePlateOutputBean {
	private List<LicensePlate> licensePlates;

	public List<LicensePlate> getLicensePlates() {
		return licensePlates;
	}

	public void setLicensePlates(List<LicensePlate> licensePlates) {
		this.licensePlates = licensePlates;
	}

}
