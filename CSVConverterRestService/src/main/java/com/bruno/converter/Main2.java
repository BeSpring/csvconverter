package com.bruno.converter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.bruno.converter.services.beans.Document;
import com.bruno.converter.services.document.DocumentService;
import com.bruno.converter.services.document.beans.DocumentOutputBean;
import com.bruno.converter.services.headers.ana.RegistryHeaders;
import com.bruno.converter.services.registry.RegistryService;
import com.bruno.converter.services.registry.beans.Registry;
import com.bruno.converter.util.GetColumnHeader;
import com.bruno.converter.util.JsonParse;
import com.bruno.createfile.util.CSVUtils;

public class Main2 {
	public static void main(String[] args) throws IOException, ParseException {

		String json_1 = "C:/Users/Bruno Baldassare/git/csvconverter/CSVConverterRestService/src/main/resources/provaANA.json";

		JsonParse jsonParse = new JsonParse();
		JSONObject object_1 = jsonParse.parseJson(json_1);

		String section_1 = (String) object_1.get("section");

		String savePathCSV_1 = "C:/Users/Bruno Baldassare/git/csvconverter/CSVConverterRestService/src/main/resources/"
				+ object_1.get("ndg") + object_1.get("cf") + ".csv";

		String type_1 = ((String) object_1.get("type")).toUpperCase();
		switch (type_1) {
		case "CSV":

			System.out.println("CSV");
			FileWriter writer = new FileWriter(savePathCSV_1);
			switch (section_1) {
			case "ANA": {
				System.out.println("ANA");

				JSONArray array = (JSONArray) object_1.get("sheetList");
				List<String> sheets = new ArrayList<>();
				for (int i = 0; i < array.size(); i++) {
					sheets.add((String) array.get(i));
				}

				for (String sheet : sheets) {

					JSONArray array22 = (JSONArray) object_1.get(sheet);
					System.out.println(sheet);
					if (array22 != null) {
//						GetColumnHeader getColumnHeader = new GetColumnHeader();
//						List<String> intSheets = getColumnHeader.getHeader(array22);
//						System.out.println(intSheets.toString());
//						CSVUtils.writeLine(writer, intSheets, ';');

						for (int i = 0; i < array22.size(); i++) {
							JSONObject obj1 = (JSONObject) array22.get(i);


							switch (sheet.toUpperCase()) {
							case "REGISTRY":
								if (i == 0) {
									GetColumnHeader getColumnHeader = new GetColumnHeader();
									List<String> intSheets = getColumnHeader.getHeader(RegistryHeaders.values());
									System.out.println(intSheets.toString());
									CSVUtils.writeLine(writer, intSheets, ';');
								}
								Registry registry = new Registry();
								List<String> registries = new ArrayList<>();

								RegistryService registryService = new RegistryService();
								registry = registryService.callRegistryService(obj1);
								registries.add(registry.getBirthplace());
								registries.add(registry.getCitizenship());
								registries.add(registry.getCompanyName());
								registries.add(registry.getDateBirth());
								registries.add(registry.getDateDeath());
								registries.add(registry.getExtendedName());
								registries.add(registry.getGender());
								registries.add(registry.getMaritalStatus());
								registries.add(registry.getMatchTypes());
								registries.add(registry.getMatrimonialRegime());
								registries.add(registry.getNickname());
								registries.add(registry.getNoResidentCode());
								registries.add(registry.getStateOfLife());
								registries.add(registry.getSurnameAcquired());
								registries.add(registry.getTaxCode());
								registries.add(registry.getType());
								registries.add(registry.getUniqueId());
								registries.add(registry.getVatNumber());
								CSVUtils.writeLine(writer, registries, ';');

								break;
							case "DOCUMENT":
								DocumentService documentService = new DocumentService();
								DocumentOutputBean documentOutputBean = documentService.callDocumentService(obj1);
								for (Document document : documentOutputBean.getDocuments()) {
									List<String> documents = new ArrayList<>();
									documents.add(document.getDateExpiration());
									documents.add(document.getDateOfIssue());
									documents.add(document.getIssuingBody());
									documents.add(document.getNationality());
									documents.add(document.getNumber());
									documents.add(document.getPlaceOfIssue());
									documents.add(document.getType());
									CSVUtils.writeLine(writer, documents, ';');
								}

								break;

							default:
								break;
							}
//							List<String> li = new ArrayList<String>();
//							JSONObject obj1 = (JSONObject) array22.get(i);
//							System.out.println(array22.get(i).toString());
//							StringBuilder stringBuilder = new StringBuilder();
//							for (String key : intSheets) {
//
//								String s = (String) obj1.get(key);
//								stringBuilder.append(s + ";");
//
//							}
//							li.add(String.valueOf(stringBuilder));
//							System.out.println(li.toString());
//							CSVUtils.writeLine(writer, li, '\n');
//
						}
					}

				}

				break;
			}
			case "PRO": {

				System.out.println("PRO");

				break;
			}

			case "INFO": {

				System.out.println("INFO");
				break;
			}

			}
			writer.flush();
			writer.close();
			System.out.println("File created in " + savePathCSV_1);
			break;

		case "XLS":
// filereader --> baseProntaXLSX
			System.out.println("EXCEL");
			break;
		}

	}
}
