package com.bruno.converter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.bruno.converter.services.CSVConvert;
import com.bruno.converter.util.JsonParse;
import com.bruno.createfile.util.CSVUtils;

public class Main {
	public static void main(String[] args) throws IOException, ParseException {
//		String json = "C:/Users/Bruno Baldassare/git/csvconverter/CSVConverterRestService/src/main/resources/input_1.json";
//		String dest = "C:/Users/Bruno Baldassare/git/csvconverter/CSVConverterRestService/src/main/resources/newR.csv";
//		JsonParse jsonParse = new JsonParse();
//		JSONObject object = jsonParse.parseJson(json);
//		CSVConvert csvConvert = new CSVConvert();
//		System.out.println(csvConvert.createCSV(object, dest));

		String json_1 = "C:/Users/Bruno Baldassare/git/csvconverter/CSVConverterRestService/src/main/resources/input_1.json";
		String json_1_bis = "C:/Users/Bruno Baldassare/git/csvconverter/CSVConverterRestService/src/main/resources/input_1_bis.json";

		JsonParse jsonParse = new JsonParse();
		JSONObject object_1 = jsonParse.parseJson(json_1);

		JsonParse jsonParseBis = new JsonParse();
		JSONObject object_1_bis = jsonParseBis.parseJson(json_1_bis);

		String section_1 = (String) object_1.get("section");
		String section_1_bis = (String) object_1_bis.get("section");

		List<List<String>> rows = new ArrayList<>();

		String savePathCSV_1 = "C:/Users/Bruno Baldassare/git/csvconverter/CSVConverterRestService/src/main/resources/"
				+ object_1.get("ndg") + object_1.get("cf") + ".csv";
		String savePathXLS_1_bis = "C:/Users/Bruno Baldassare/git/csvconverter/CSVConverterRestService/src/main/resources/"
				+ object_1_bis.get("ndg") + object_1_bis.get("cf") + ".xlsx";
//		switch (section_1_bis) {
//		case "ANA": {
//			// param "ndg"
//			// call ANAService
//			System.out.println("ANA");
//			CSVConvert csvConvert = new CSVConvert();
//			csvConvert.createCSV(object_1, "")
//
//			// OutputBeanInfo otbin=return call;
//			break;
//		}
//		case "PRO": {
//			// call PROService
//			// OutputBeanInfo otbin=return call;
//			System.out.println("PRO");
//			
//			
//			break;
//		}
//
//		case "INFO": {
//			// call INFOService
//			// OutputBeanInfo otbin=return call;
//			
//			System.out.println("INFO");
//			break;
//		}
//
//		}

//		String type_1_bis = ((String) object_1_bis.get("type")).toUpperCase();
		String type_1 = ((String) object_1.get("type")).toUpperCase();
		switch (type_1) {
		case "CSV":
//PRIMA OPZIONE INSTANCE OF 

			// if Object instance of Output
			System.out.println("CSV");
			FileWriter writer = new FileWriter(savePathCSV_1);
			switch (section_1) {
			case "ANA": {
				// param "ndg"
				// call ANAService
				System.out.println("ANA");

				JSONArray array = (JSONArray) object_1.get("sheetList");
				List<String> sheets = new ArrayList<>();
				for (int i = 0; i < array.size(); i++) {
					sheets.add((String) array.get(i));
				}

				for (String sheet : sheets) {

					JSONArray array22 = (JSONArray) object_1.get(sheet);
					System.out.println(sheet);
					if (array22 != null)

						for (int i = 0; i < array22.size(); i++) {
							List<String> li = new ArrayList<String>();
							JSONObject obj1 = (JSONObject) array22.get(i);
							Set keySet = obj1.keySet();
							System.out.println(keySet);
							System.out.println(array22.get(i).toString());
							StringBuilder stringBuilder = new StringBuilder();
							for (int j = 1; j < obj1.size(); j++) {

								String s = (String) obj1.get("col" + j);
								stringBuilder.append(s + ";");

							}
							li.add(String.valueOf(stringBuilder));
							rows.add(li);

						}
					else
						continue;
//				FileWriter writer = new FileWriter(savePathCSV_1);
					for (List<String> s : rows) {
						for (String str : s) {
							List<String> list = new ArrayList<>();
							list.add(str);
							CSVUtils.writeLine(writer, list);
							list.clear();
						}

					}

				}

//				writer.flush();
//				writer.close();

				// OutputBeanInfo otbin=return call;
				break;
			}
			case "PRO": {
				// call PROService
				// OutputBeanInfo otbin=return call;
				System.out.println("PRO");

				break;
			}

			case "INFO": {
				// call INFOService
				// OutputBeanInfo otbin=return call;

				System.out.println("INFO");
				break;
			}

			}
			writer.flush();
			writer.close();
			System.out.println("File created in " + savePathCSV_1);
			break;

		case "XLS":
// filereader --> baseProntaXLSX
			System.out.println("EXCEL");
			break;
		}

	}
}
