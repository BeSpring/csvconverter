package com.bruno.converter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsvConverterRestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsvConverterRestServiceApplication.class, args);
	}

}
